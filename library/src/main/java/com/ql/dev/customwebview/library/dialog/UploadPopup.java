package com.ql.dev.customwebview.library.dialog;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.ql.dev.customwebview.library.R;
import com.ql.dev.customwebview.library.utils.StringUtils;

/**
 *
 * Created by Administrator on 2016/7/15.
 */
public class UploadPopup {

    private static final String TAG = UploadPopup.class.getSimpleName();
    public static UploadPopup instances;
    private Activity mContext;
    private int llUpdateCenter;
    private TextView tvTitle;
    private TextView tvChoosePitcure;
    private TextView tvTakePhoto;
    private TextView tvcancelPop;
    private boolean isShowTitle;
    private String titleValue;
    private boolean isShowFirst;
    private String firstValue;
    private String secValue;
    private String cacleValue;

    public void setValues(boolean isShowTitle, String titleValue, boolean isShowFirst, String firstValue, String secValue, String cacleValue) {
        this.isShowTitle = isShowTitle;
        this.titleValue = titleValue;
        this.isShowFirst = isShowFirst;
        this.firstValue = firstValue;
        this.secValue = secValue;
        this.cacleValue = cacleValue;
    }

    public interface UploadListener {
        void onClickChooseCancel();

        void onClickChoosePicListener();

        void onClickTaskPhotoListener();
    }

    private UploadListener mUploadListener;

    /**
     * @param mContext
     * @param llUpdateCenter 在哪个控件的下方
     */
    public UploadPopup(Activity mContext, int llUpdateCenter, UploadListener uploadListener) {
        instances = this;
        this.mContext = mContext;
        this.llUpdateCenter = llUpdateCenter;
        this.mUploadListener = uploadListener;
    }

    /**
     * 上传图片的popwindow
     */
    public void initPopWindowUploadHeader() {
        // 利用layoutInflater获得View
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.pop_upload_header, null);

        // 下面是两种方法得到宽度和高度 getWindow().getDecorView().getWidth()

        final PopupWindow window = new PopupWindow(view,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);

        // 设置popWindow弹出窗体可点击，这句话必须添加，并且是true
        window.setFocusable(true);
        //设置点击背景消失
        window.setOutsideTouchable(true);
        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        window.setBackgroundDrawable(dw);

        // 设置popWindow的显示和消失动画
        window.setAnimationStyle(R.style.mypopwindow_anim_style);
        // 在底部显示
        window.showAtLocation(mContext.findViewById(llUpdateCenter), Gravity.BOTTOM, 0, 0);

        // 设置背景颜色变暗
//        WindowManager.LayoutParams lp = mContext.getWindow().getAttributes();
//        lp.alpha = 0.8f;
//        mContext.getWindow().setAttributes(lp);

        window.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                WindowManager.LayoutParams lp = mContext.getWindow().getAttributes();
                lp.alpha = 1f;
                mContext.getWindow().setAttributes(lp);
            }
        });
        /**
         * 给popwinsow以外的部分点击事件
         * 如果显示了就消失
         */
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (window.isShowing()) {
                    window.dismiss();
                }
            }
        });
        tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        tvChoosePitcure = (TextView) view.findViewById(R.id.tvChoosePitcure);
        tvTakePhoto = (TextView) view.findViewById(R.id.tvTakePhoto);
        tvcancelPop = (TextView) view.findViewById(R.id.tvcancelPop);
        if (!isShowTitle) {
            tvTitle.setVisibility(View.GONE);
        } else if (!StringUtils.isEmpty(titleValue)) {
            tvTitle.setText(titleValue);
            tvTitle.setVisibility(View.VISIBLE);
        }
        if (!isShowFirst) {
            tvChoosePitcure.setVisibility(View.GONE);
        } else if (!StringUtils.isEmpty(firstValue)) {
            tvChoosePitcure.setText(firstValue);
        }
        if (!StringUtils.isEmpty(secValue)) {
            tvTakePhoto.setText(secValue);
        }
        if (!StringUtils.isEmpty(cacleValue)) {
            tvcancelPop.setText(cacleValue);
        }
        /**
         * 取消
         */
        tvcancelPop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                window.dismiss();
                mUploadListener.onClickChooseCancel();
            }
        });
        /**
         * 查看图片
         */
        tvChoosePitcure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                window.dismiss();
                taskCamcen();
            }
        });
        /**
         * 保存图片
         */
        tvTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                window.dismiss();
                taskPhoto();
            }
        });
    }


    public void taskPhoto() {
        mUploadListener.onClickTaskPhotoListener();
    }


    private void taskCamcen() {
        mUploadListener.onClickChoosePicListener();
    }

    /**
     * @param uri
     * @return
     */
    public Uri onPhotoCropped(Uri uri) {
        Log.d(TAG, "Crop Uri in path: " + uri.getPath());
        //logoBitmap = CropHelper.decodeUriAsBitmap(mContext, mCropParams.uri);
        //imageView.setImageBitmap(logoBitmap);
        //storage/emulated/0/crop_cache_file.jpg
        //file = new File(URI.create(uri.toString()));
        return uri;
    }
}
