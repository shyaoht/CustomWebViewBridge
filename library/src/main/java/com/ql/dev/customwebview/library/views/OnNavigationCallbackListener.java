package com.ql.dev.customwebview.library.views;

import android.app.Activity;
import android.view.View;
import android.webkit.WebView;

import com.ql.dev.customwebview.library.dialog.UploadPopup;

/**
 * Created by Quanguanzhou on  2017/3/23 0023.
 * Copyright (c) 2015, quanguanzhou83@gmail.com All Rights Reserved.
 */
public abstract class OnNavigationCallbackListener {

    /**
     * 处理导航条左测返回按钮点击事件
     */
    public void doLeftbackListener(WebView webView, Activity activity){
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            activity.finish();
        }
    }

    /**
     * 处理导航条关闭按钮点击事件
     * @param webView
     */
    public void doLeftCloseListener(WebView webView,Activity activity){
        activity.finish();
    }

    /**
     * 处理成右测刷新按钮点击事件
     * @param webView
     */
    public void doRightCallbackListener(WebView webView,Activity activity){
        webView.reload();
    }
}
