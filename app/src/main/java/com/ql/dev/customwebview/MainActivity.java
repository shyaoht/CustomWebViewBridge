package com.ql.dev.customwebview;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.ql.dev.customwebview.demo.CustomWebViewActivity;
import com.ql.dev.library.TitleNavView;

/**
 *
 * Created by Administrator on 2018-10-26 0026.
 */
public class MainActivity extends AppCompatActivity {
    private TitleNavView titleNav;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        titleNav = findViewById(R.id.titleNav);
        titleNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CustomWebViewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("url", "file:///android_asset/index.html");
                //bundle.putString("url", "http://www.baidu.com");
                bundle.putString("centerTitle", "内置本地网页");
                bundle.putInt("backPic", R.mipmap.icon_back);
                bundle.putInt("backColor", R.color.common_blue);
                bundle.putInt("backfontColor", R.color.white);
                bundle.putInt("closeFontColor", R.color.white);
                bundle.putInt("centerFontColor", R.color.white);
                bundle.putInt("rightFontColor", R.color.white);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }
}
