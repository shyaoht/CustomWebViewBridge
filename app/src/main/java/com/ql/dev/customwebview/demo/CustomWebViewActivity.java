package com.ql.dev.customwebview.demo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;

import com.ql.dev.customwebview.R;
import com.ql.dev.customwebview.library.views.CustomWebView;
import com.ql.dev.customwebview.library.views.OnNavigationCallbackListener;
import com.ql.dev.customwebview.library.views.OnWebViewClientListener;

/**
 * Created by Quanguanzhou on  2017/3/23 0023.
 * Copyright (c) 2015, quanguanzhou83@gmail.com All Rights Reserved.
 */
public class CustomWebViewActivity extends AppCompatActivity {
    /**
     * 显示的Url页面
     */
    private String url;
    /**
     * 页面的标题
     */
    private String centerTitle;
    /**
     * 字体颜色
     */
    private int backfontColor;

    private int closeFontColor;

    private int centerFontColor;

    private int rightFontColor;
    /**
     * 返回的图片
     */
    private int backPic;
    /**
     * 背景颜色
     */
    private int backColor;

    private String isShowBack; // 1是0否

    private String isShowClose;// 1是0否

    private String isRefresh;// 1是0否

    private CustomWebView.Builder builder;
    private View contentView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contentView = getLayoutInflater().inflate(R.layout.activity_custom_webview, null);
        setContentView(contentView);
        initContentView(contentView,savedInstanceState);
    }


    public void initContentView(View contentView, Bundle savedInstanceState) {
        initReceverData();
        builder = new CustomWebView.Builder(this, (ViewGroup) contentView);
        builder.setPageCenterTitle(centerTitle).setBackColor(backColor);
        builder.setBackPic(backPic).setBackFontColor(backfontColor);
        builder.setCloseFontColor(closeFontColor).setCenterFontColor(centerFontColor);
        builder.setRightFontColor(rightFontColor).setUrl(url);
        builder.setIsShowBack(isShowBack).setIsShowClose(isShowClose).setIsRefresh(isRefresh);
        builder.setNavigationCallbackListener(new OnNavigationCallbackListener() {
            @Override
            public void doLeftbackListener(WebView webView, Activity activity) {
                super.doLeftbackListener(webView, activity);
            }

            @Override
            public void doLeftCloseListener(WebView webView, Activity activity) {
                super.doLeftCloseListener(webView, activity);
            }

            @Override
            public void doRightCallbackListener(WebView webView, Activity activity) {
                super.doRightCallbackListener(webView, activity);
            }
        });
        builder.setOnWebViewClientListener(new OnWebViewClientListener() {
            @Override
            public void onPageFinished(WebView view, String url) {

            }
        });
        builder.setHostJsScopeClass(CustomHostJsScope.class); // 设置自定义的Js与Java的交互处理类
        builder.create();
    }

    private void initReceverData() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            backfontColor = bundle.getInt("backfontColor", R.color.white);
            closeFontColor = bundle.getInt("closeFontColor", R.color.white);
            centerFontColor = bundle.getInt("centerFontColor", R.color.white);
            rightFontColor = bundle.getInt("rightFontColor", R.color.white);
            backPic = bundle.getInt("backPic", R.mipmap.icon_back);
            backColor = bundle.getInt("backColor", R.color.common_blue);
            url = bundle.getString("url","www.baidu.com");
            isShowBack = bundle.getString("isShowBack","1");
            isShowClose = bundle.getString("isShowClose","1");
            isRefresh = bundle.getString("isRefresh","1");
            centerTitle = bundle.getString("centerTitle","");
        }
    }

    /**
     * 监听用户手机按键，如果打开的网址可以返回，
     * 则返回，如果不可以返回则正常处理
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        WebView webView = builder.getWebView();
        if(webView!=null) {
            if ((keyCode == KeyEvent.KEYCODE_BACK) && builder.getWebView().canGoBack()) {
                builder.getWebView().goBack();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        builder.onDestroy();
    }
}
